package buu.bowornchat.plusgame

data class MyString(var app_name: String = "",
                  var plus: String = "",
                  var equal: String = "",
                  var corect: String = "",
                  var incorect: String = "",
                  var numCorect: String = "",
                  var numInCorect: String = "",
                  var stringCorect: String = "",
                  var stringIncorect: String = "")