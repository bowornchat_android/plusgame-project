package buu.bowornchat.plusgame

import android.os.Bundle
import android.renderscript.Sampler
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import buu.bowornchat.plusgame.databinding.ActivityMainBinding
import java.lang.Integer.parseInt
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

    }

}