package buu.bowornchat.plusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.bowornchat.plusgame.databinding.ActivityMainBinding
import buu.bowornchat.plusgame.databinding.FragmentGameBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GameFragment : Fragment() {
    private val myString: MyString = MyString("PlusGame","+","=","","ไม่ถูกต้อง","0","0","ถูก :","ผิด :")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = DataBindingUtil.inflate<FragmentGameBinding>(inflater,
            R.layout.fragment_game,container,false)
        binding.apply {
            round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            btnNumber1.setOnClickListener {
                if(checkTrue(txtNum1, txtNum2, btnNumber1)){
                    txtResult.setText(R.string.corect)
                    Win(txtWin)
                }else{
                    txtResult.setText(R.string.incorect)
                    Lose(txtLose)
                }
                round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            }
            btnNumber2.setOnClickListener {
                if(checkTrue(txtNum1,txtNum2,btnNumber2)){
                    txtResult.setText(R.string.corect)
                    Win(txtWin)
                }else{
                    txtResult.setText(R.string.incorect)
                    Lose(txtLose)
                }
                round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            }
            btnNumber3.setOnClickListener {
                if(checkTrue(txtNum1,txtNum2,btnNumber3)){
                    txtResult.setText(R.string.corect)
                    Win(txtWin)
                }else{
                    txtResult.setText(R.string.incorect)
                    Lose(txtLose)
                }
                round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            }
        }
        binding.myString = myString
        return binding.root
    }
    fun round(txtnumber1: TextView, txtnumber2: TextView, btnNumber1: Button, btnNumber2: Button, btnNumber3: Button) {
        txtnumber1.text  =randomNumber().toString()
        txtnumber2.text  =randomNumber().toString()
        val result  = Integer.parseInt(txtnumber1.text.toString()) + Integer.parseInt(txtnumber2.text.toString())
        when (randomButton()) {
            0 -> {
                btnNumber1.text = result.toString()
                btnNumber2.text = (result+1).toString()
                btnNumber3.text = (result+2).toString()
            }
            1 -> {
                btnNumber1.text = (result-1).toString()
                btnNumber2.text = result.toString()
                btnNumber3.text = (result+1).toString()
            }
            2 -> {
                btnNumber1.text = (result-2).toString()
                btnNumber2.text = (result-1).toString()
                btnNumber3.text = result.toString()
            }
        }
    }
    fun checkTrue(txtnumber1: TextView, txtnumber2 : TextView, btnNumber: Button): Boolean {
        val result  = Integer.parseInt(txtnumber1.text.toString()) + Integer.parseInt(txtnumber2.text.toString())
        if(result == Integer.parseInt(btnNumber.text.toString())){
            return true
        }
        return false
    }
    fun randomNumber(): Int {
        return (1..10).random()
    }
    fun randomButton(): Int {
        return (0..2).random()
    }
    fun Win(txtWin: TextView) {
        txtWin.text = (Integer.parseInt(txtWin.text.toString()) +1).toString()
    }
    fun Lose(txtLose: TextView) {
        txtLose.text = (Integer.parseInt(txtLose.text.toString()) +1).toString()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GameFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            GameFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}